#include <game-server-library/messages.h>
#include <algorithm>
#include <assert.h>

/*static */unsigned int Message::stou(const std::string &str)
{
    return static_cast<unsigned int>(stoul(str));
}


/*static */unsigned int Message::getNumber(std::string &message, const std::string &delimiter/* = " "*/)
{
    unsigned int number;
    if (message[0] == ' ') {
        message = message.substr(1, message.size());
    }
    std::string token = message.substr(0, message.find(delimiter));
    if (token == "")
        number = Message::stou(message);
    else
        number = Message::stou(token);
    message = message.substr(token.size(), message.size());

    return number;
}


/*static */std::string Message::getString(std::string &message, const std::string &delimiter/* = " "*/)
{
    if (message[0] == ' ')
        message = message.substr(1, message.size());

    std::string token = message.substr(0, message.find(delimiter));
    message = message.substr(token.size(), message.size());

    return token;
}

/*static */const Message* Message::createForServer(const std::string &msg)
{
    std::string message = msg;
    std::string token = getString(message);

    // c<client-id> <string-to-deserialize>
    if (token.size() != 2 || token[0] != 'C' || (token[1] != '1' && token[1] != '2'))
        return nullptr;

    int client_id = token[1] - '1';

    // The only message the server gets from a client is MSG_ClientMove
    Message* m = new MSG_ClientMove(client_id);
    if (!m->deserialize(message)) {
        delete m;
        m = nullptr;
    }

    return m;
}

/*static */const Message* Message::createForClient(const std::string &msg)
{
    std::string message = msg;
//    std::string token = getString(message);

    Message* m = new MSG_Land();
    if (!m->deserialize(message)) {
        delete m;
        m = nullptr;
    }
    else {
        return m;
    }

    m = new MSG_GameStatus();
    if (!m->deserialize(message)) {
        delete m;
        m = nullptr;
    }
    else {
        return m;
    }

    return nullptr;
}

/*virtual */Message::~Message()
{
}


MSG_ClientMove::MSG_ClientMove(Actions action, const Direction &direction/* = Direction()*/)
    : Message()
    , _action(action)
    , _direction(direction)
{
    assert( (action == Actions::MOVE && direction.isValid())
           || (action == Actions::EAT && !direction.isValid()) );
}


MSG_ClientMove::MSG_ClientMove(int client_id)
    : Message(client_id)
{
}


std::string MSG_ClientMove::serialize() const
{
    std::string msg = "";
    if (_action == MOVE) {
        msg += "move ";
        msg += std::to_string(_direction.direction());
    }
    else if (_action == EAT) {
        msg += "eat";
    }

    return msg;
}


bool MSG_ClientMove::deserialize(const std::string &msg)
{
    std::string message = msg;
    std::string act = getString(message);

    if (act == "move") {
        _action = MOVE;

        unsigned int direct = getNumber(message);
        _direction = Direction(static_cast<Direction::Dir>(direct));

        return _direction.isValid() && message.empty();
    }
    else if (act == "eat") {
        _action = EAT;
        return message.empty();
    }

    return false;
}


MSG_GameStatus::MSG_GameStatus(int client_id, const unsigned int &turn, const Cells &food_added, const Cells &food_eaten, const Player &me, const Player &opp)
    :Message(client_id)
    , _turn(turn)
    , _food_added(food_added), _food_eaten(food_eaten)
    , _me(me), _opp(opp)
{
}


std::string MSG_GameStatus::serialize() const
{
    std::string msg = "";
    msg += "C" + std::to_string(clientId()+1) + " ";

    msg += "turn " + std::to_string(_turn) + " ";
    msg += "food " + std::to_string(_food_added.size()) + " ";
    for (const auto &fa : _food_added) {
        msg += std::to_string(fa.x()) + " "+ std::to_string(fa.y()) + " ";
    }

    msg += std::to_string(_food_eaten.size()) + " ";
    for (const auto &fe : _food_eaten) {
        msg += std::to_string(fe.x()) + " "+ std::to_string(fe.y()) + " ";
    }

    msg += "me " + std::to_string(_me.x()) + " " + std::to_string(_me.y()) + " " + std::to_string(_me.rating()) + " ";
    msg += "opp " + std::to_string(_opp.x()) + " " + std::to_string(_opp.y()) + " " + std::to_string(_opp.rating());

    return msg;
}


bool MSG_GameStatus::deserialize(const std::string &msg)
{
    std::string message = msg;
    std::string token = getString(message);

    if (token == "turn") {
        _turn = getNumber(message);
    }
    else {
        return false;
    }

    token = getString(message);
    if (token == "food") {
        unsigned int food_added_cnt = getNumber(message);

        for (unsigned int i = 0; i < food_added_cnt; i++) {
            unsigned int x, y;
            x = getNumber(message);
            y = getNumber(message);

            _food_added.insert(Cell(x, y));
        }

        unsigned int food_eaten_cnt = getNumber(message);

        for (unsigned int i = 0; i < food_eaten_cnt; i++) {
            unsigned int x, y;
            x = getNumber(message);
            y = getNumber(message);

            _food_eaten.insert(Cell(x, y));
        }
    }
    else {
        return false;
    }

    token = getString(message);
    if (token == "me") {

        unsigned int x, y, rating;
        x = getNumber(message);
        y = getNumber(message);
        rating = getNumber(message);

        _me = Player(x, y, rating);
    }
    else {
        return false;
    }

    token = getString(message);
    if (token == "opp") {
        unsigned int x, y, rating;

        x = getNumber(message);
        y = getNumber(message);
        rating = getNumber(message);

        _opp = Player(x, y, rating);
    }
    else {
        return false;
    }

    return true;
}


std::string MSG_Land::serialize() const
{
    std::string msg = "";
    msg += "C" + std::to_string(clientId()+1) + " ";
    msg += std::to_string(_x) + " ";
    msg += std::to_string(_y);

    return msg;
}


bool MSG_Land::deserialize(const std::string &msg)
{
    std::string message = msg;
    if (msg.size() >= 4 && msg.substr(0, 4) == "turn") {
        return false;
    }
    _x = getNumber(message);
    _y = getNumber(message);

    return true;
}
