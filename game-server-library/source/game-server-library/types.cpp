#include <game-server-library/types.h>
#include <assert.h>



//------------
// class Size
//------------

bool Size::operator==(const Size &s) const
{
    return isValid() && s.isValid()
            && _width == s._width
            && _height == s._height;
}



//------------
// class Cell
//------------

/*static */Size Cell::_board_size;

Cell::Cell(int index)
    : _x(index % _board_size.width()), _y(index / _board_size.width())
{
    assert(index >= 0);
}

/*static */Cell Cell::createRandom(const Cells &ordered_cells)
{
    if (_board_size.area() <= ordered_cells.size()) {
        return Cell();
    }

    unsigned int cells_count =
            _board_size.area() - static_cast<unsigned int>(ordered_cells.size());
    int index = rand() % cells_count;
    for (const auto &c : ordered_cells) {
        if (index >= c.mapToIndex()) {
            index++;
        } else {
            break;
        }
    }

    return Cell(index);
}

bool Cell::operator==(const Cell &c) const
{
    return isValid() && c.isValid()
            && _x == c._x
            && _y == c._y;
}

bool Cell::operator<(const Cell &c) const
{
    assert(isValid() && c.isValid());
    return mapToIndex() < c.mapToIndex();
}


Cell &Cell::operator+=(const Direction &direction)
{
    assert(direction.isValid());
    _x = ( _x + direction.dx() + static_cast<int>(_board_size.width()) ) % _board_size.width();
    _y = ( _y + direction.dy() + static_cast<int>(_board_size.height()) ) % _board_size.height();
    return *this;
}

Cell &Cell::operator-=(const Direction &direction)
{
    assert(direction.isValid());
    _x = ( _x - direction.dx() + static_cast<int>(_board_size.width()) ) % _board_size.width();
    _y = ( _y - direction.dy() + static_cast<int>(_board_size.height()) ) % _board_size.height();
    return *this;
}

bool Cell::isValid() const
{
    return _x >= 0 && _x < static_cast<int>(_board_size.width())
            && _y >= 0 && _y < static_cast<int>(_board_size.height());
}

/*static */void Cell::setBoardSize(const Size &size)
{
    assert(size.isValid());
    _board_size = size;
}



//--------------
// class Direction
//--------------

/*static */std::pair<int, int> Direction::_deltas[Dir::MAX] = {
    {  0,  0 },    // STOP
    { -1,  0 },    // LEFT
    {  0, +1 },    // UP
    { +1,  0 },    // RIGHT
    {  0, -1 },    // DOWN
};

/*static */Direction Direction::createSafe(Dir direction)
{
    Dir safe_direction = (direction >= Dir::STOP && direction < Dir::MAX)
            ? direction
            : Dir::STOP;
    return Direction(safe_direction);
}

bool Direction::operator==(const Direction &m) const
{
    return isValid() && m.isValid()
            && _direction == m._direction;
}

int Direction::dx() const
{
    return Direction::_deltas[safeDirection()].first;
}

int Direction::dy() const
{
    return Direction::_deltas[safeDirection()].second;
}
