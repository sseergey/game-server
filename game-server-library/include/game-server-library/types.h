#pragma once

#include <set>
#include <string>

class Cell;
typedef std::set<Cell> Cells;
typedef Cells::iterator CellsIterator;
class Direction;


class Size
{
public:
    Size() { }
    Size(unsigned int width, unsigned int height) : _width(width), _height(height) { }
    bool operator==(const Size &s) const;
    bool operator!=(const Size &s) const { return *this == s; }

    unsigned int width() const { return _width; }
    unsigned int height() const { return _height; }
    unsigned int area() const { return _width * _height; }
    bool isValid() const { return _width > 0 && _height > 0; }

private:
    unsigned int _width = 0;
    unsigned int _height = 0;
};


class Cell
{
public:
    Cell() { }
    Cell(int x, int y) : _x(x), _y(y) { }
    Cell(unsigned int x, unsigned int y) : _x(static_cast<int>(x)), _y(static_cast<int>(y)) { }
    Cell(int index);
    static Cell createRandom(const Cells &ordered_cells);
    bool operator==(const Cell &c) const;
    bool operator!=(const Cell &c) const { return *this == c; }
    bool operator<(const Cell &c) const;

    Cell &operator+=(const Direction &direction);
    Cell &operator-=(const Direction &direction);

    int x() const { return _x; }
    int y() const { return _y; }
    bool isValid() const;

    static void setBoardSize(const Size &size);

protected:
    int mapToIndex() const { return _y * static_cast<int>(_board_size.width()) + _x; }

private:
    int _x = -1;
    int _y = -1;
    static Size _board_size;
};


class Direction
{
public:
    enum Dir {
        STOP,
        LEFT,
        UP,
        RIGHT,
        DOWN,
        MAX
    };

public:
    Direction() { }
    Direction(Dir direction) : _direction(direction) { }
    static Direction createSafe(Dir direction);
    bool operator==(const Direction &m) const;
    bool operator!=(const Direction &m) const { return *this == m; }

    Dir direction() const { return _direction; }
    Dir safeDirection() const { return isValid() ? _direction : Dir::STOP; }

    bool isValid() const { return _direction >= Dir::STOP && _direction < Dir::MAX; }
    int dx() const;
    int dy() const;

private:
    Dir _direction = Dir::MAX;
    static std::pair<int, int> _deltas[Dir::MAX];
};
