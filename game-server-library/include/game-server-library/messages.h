#pragma once

#include <string>
#include <vector>
#include "types.h"

class Message
{
public:

    enum Actions {
        MOVE = 0,
        EAT = 1
    };

    static const Message* createForServer(const std::string &msg);
    static const Message* createForClient(const std::string &msg);

    int clientId() const { return _client_id; }

    virtual std::string serialize() const = 0;
    virtual bool deserialize(const std::string &msg) = 0;

protected:
    Message() { }
    Message(int client_id)
        : _client_id(client_id)
    { }
    Message(const Message &) = delete;
    Message &operator=(const Message &) = delete;

    virtual ~Message();

    static unsigned int stou(const std::string &str);
    static unsigned int getNumber(std::string &message, const std::string &delimiter = " ");
    static std::string getString(std::string &message, const std::string &delimiter = " ");

private:
    int _client_id = -1;
};


class MSG_ClientMove : public Message
{
friend const Message* Message::createForServer(const std::string &msg);
public:

    MSG_ClientMove(Actions action, const Direction &direction = Direction());
    std::string serialize() const;
    bool deserialize(const std::string &msg);
    Actions action() const { return _action; }
    Direction direction() const { return _direction; }

protected:
    // To deserialize
    MSG_ClientMove(int client_id);

private:
    Actions _action;
    Direction _direction;
};


class Player
{
public:

    Player(unsigned int x, unsigned int y, unsigned int rating)
      : _coord(x, y)
      , _rating(rating)
    {}
    Player() { }

    int x() const { return _coord.x(); }
    int y() const { return _coord.y(); }

    unsigned int& rating() { return _rating; }
    const unsigned int& rating() const { return _rating; }

    Cell& coord() { return _coord; }
    const Cell& coord() const { return _coord; }

private:
    Cell _coord;
    unsigned int _rating;
};


class MSG_GameStatus : public Message
{
friend const Message* Message::createForClient(const std::string &msg);
public:
    std::string serialize() const;
    bool deserialize(const std::string &msg);
    MSG_GameStatus(int client_id, const unsigned int &turn, const Cells &food_added, const Cells &food_eaten, const Player &me, const Player &opp);

    unsigned int turn() const { return _turn; }
    Cells food_added() const { return _food_added; }
    Cells food_eaten() const { return _food_eaten; }
    const Player &me() const { return _me; }
    const Player &opp() const { return _opp; }

protected:
    // To deserialize()
    MSG_GameStatus() { }

private:
    unsigned int _turn;

    Cells _food_added;
    Cells _food_eaten;

    Player _me;
    Player _opp;
};


class MSG_Land : public Message
{
    friend const Message* Message::createForClient(const std::string &msg);
public:
    std::string serialize() const;
    bool deserialize(const std::string &msg);
    MSG_Land(int client_id, unsigned int x, unsigned int y) :Message(client_id), _x(x), _y(y) {}
    unsigned int x() const { return _x; }
    unsigned int y() const { return _y; }

protected:
    // To deserialize()
    MSG_Land() { }

private:
//    Size _size;
    unsigned int _x;
    unsigned int _y;
};
