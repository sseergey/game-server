#include "server.h"
#include <assert.h>


int main()
{
    srand(0);
    Server server;

    while (!server.isGameEnd()) {
        bool res = server.makeTurn();
        assert(res);
    }
}
