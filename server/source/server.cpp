#include "server.h"
#include "log.h"
#include <iostream>
#include <algorithm>
#include <assert.h>


Server::Server()
{
    logInit(LOG_FILENAME);

    Cell::setBoardSize({ width, height });
    _clients[Consts::FIRST_CLIENT].coord() = startPosClientOne;
    _clients[Consts::LAST_CLIENT].coord() = startPosClientTwo;

    _turn = Consts::STANDARD_TURNS_COUNT;
    for (int i = 0; i < Consts::CLIENT_COUNT; i++) {
        _clients[i].rating() = 0;
    }
    generateInitialFood();

    MSG_Land landFirst(Consts::FIRST_CLIENT, width, height);
    SendLand(landFirst);
    MSG_Land landLast(Consts::LAST_CLIENT, width, height);
    SendLand(landLast);

    Cells food = _food;
    MSG_GameStatus game[Consts::CLIENT_COUNT] = {
        { Consts::FIRST_CLIENT, _turn, food, Cells(), _clients[Consts::FIRST_CLIENT], _clients[Consts::LAST_CLIENT] },
        { Consts::LAST_CLIENT, _turn, food, Cells(), _clients[Consts::LAST_CLIENT], _clients[Consts::FIRST_CLIENT] }
    };
    sendChangesClient(game[Consts::FIRST_CLIENT]);
    sendChangesClient(game[Consts::LAST_CLIENT]);

}


bool Server::makeTurn()
{
    // Get response
    const MSG_ClientMove* client_moves[Consts::CLIENT_COUNT] = { nullptr, nullptr };

    for (size_t i = 0; i < Consts::CLIENT_COUNT; i++) {
        const MSG_ClientMove* client_msg = receiveClientMove();
        if (client_msg == nullptr) {
            return false;
        }

        int client_id = client_msg->clientId();
        if (client_moves[client_id] != nullptr) {
            delete client_moves[client_id];
            return false;
        }

        client_moves[client_id] = client_msg;
    }

    // Change data
    Cells foodL = _food;
    changeData(client_moves);

    for (const MSG_ClientMove* client_msg : client_moves) {
        if (client_msg != nullptr) {
            delete client_msg;
        }
    }

    _turn--;

    // Send GameStatus
    Cells food;
    std::set_difference(foodL.begin(), foodL.end(), _food.begin(), _food.end(), std::inserter(food, food.end()));
    MSG_GameStatus game[Consts::CLIENT_COUNT] = {
        { Consts::FIRST_CLIENT, _turn, Cells(), food, _clients[Consts::FIRST_CLIENT], _clients[Consts::LAST_CLIENT] },
        { Consts::LAST_CLIENT, _turn, Cells(), food, _clients[Consts::LAST_CLIENT], _clients[Consts::FIRST_CLIENT] }
    };
    sendChangesClient(game[Consts::FIRST_CLIENT]);
    sendChangesClient(game[Consts::LAST_CLIENT]);

    if (_turn == 0) {
        finalizeGame();
    }

    return true;
}


void Server::SendLand(const MSG_Land &message)
{
    std::string serialized_message = message.serialize();

    logMe("S --> %s\n", serialized_message.data());
    std::cout << serialized_message << std::endl;
}


const MSG_ClientMove* Server::receiveClientMove()
{
    std::string msg;
    getline(std::cin, msg);

    const MSG_ClientMove* message = static_cast<const MSG_ClientMove* >(Message::createForServer(msg));
    if (message == nullptr) {
        std::cerr << "err" << std::endl;
    } else {
        logMe("S <-- %s\n", msg.data());
    }

    return message;
}


void Server::sendChangesClient(const MSG_GameStatus &message)
{
    std::string serialized_message = message.serialize();

    logMe("S --> %s\n", serialized_message.data());
    std::cout << serialized_message << std::endl;
}


bool Server::changeData(const MSG_ClientMove* const client_moves[])
{
    bool res = true;

    for (size_t i = 0; i < Server::Consts::CLIENT_COUNT; i++) {
        res = moveAndEat(client_moves[i]) && res;
    }
    if (!res) {
        return false;
    }

    resolveCollision(client_moves);

    for (size_t i = 0; i < Server::Consts::CLIENT_COUNT; i++) {
        eatenFoodCleanup(client_moves[i]);
    }

    return true;
}

bool Server::moveAndEat(const MSG_ClientMove* client_move)
{
    if (client_move->action() == Message::Actions::MOVE) {
        _clients[client_move->clientId()].coord() += client_move->direction();

    } else if (client_move->action() == Message::Actions::EAT) {

        CellsIterator food_it = _food.find(_clients[client_move->clientId()].coord());
        if (food_it != _food.end()) {
            _clients[client_move->clientId()].rating() += Consts::FOOD_EAT_RATING;
        }
    } else {

        return false;
    }

    return true;
}

void Server::resolveCollision(const MSG_ClientMove* const client_moves[])
{
    if (_clients[Consts::FIRST_CLIENT].coord() == _clients[Consts::LAST_CLIENT].coord()
            && client_moves[Consts::FIRST_CLIENT]->action() == Message::Actions::EAT
            && client_moves[Consts::LAST_CLIENT]->action() == Message::Actions::EAT) {

        CellsIterator food_it = _food.find(_clients[Consts::FIRST_CLIENT].coord());
        if (food_it != _food.end()) {
            _clients[Consts::FIRST_CLIENT].rating() -= Consts::FOOD_EAT_RATING / 2;
            _clients[Consts::LAST_CLIENT].rating() -= Consts::FOOD_EAT_RATING / 2;
        }
    }
}

void Server::eatenFoodCleanup(const MSG_ClientMove* client_move)
{
    if (client_move->action() == Message::Actions::EAT) {
        CellsIterator food_it = _food.find(_clients[client_move->clientId()].coord());
        if (food_it != _food.end()) {
            _food.erase(food_it);
        }
    }
}


void Server::generateInitialFood()
{
    assert(Consts::FOOD_START_COUNT < width * height);
    _food.clear();
    for (size_t i = 0; i < Consts::FOOD_START_COUNT; i++) {
        Cell cell = Cell::createRandom(_food);
        assert(cell.isValid());
        _food.insert(cell);
    }
}


void Server::finalizeGame() const
{
    int points[Consts::CLIENT_COUNT];

    if (_clients[Consts::FIRST_CLIENT].rating() > _clients[Consts::LAST_CLIENT].rating()) {
        points[Consts::FIRST_CLIENT] = 2;
        points[Consts::LAST_CLIENT] = 0;
    } else if (_clients[Consts::FIRST_CLIENT].rating() < _clients[Consts::LAST_CLIENT].rating()) {
        points[Consts::FIRST_CLIENT] = 0;
        points[Consts::LAST_CLIENT] = 2;
    } else {
        points[Consts::FIRST_CLIENT] = points[Consts::LAST_CLIENT] = 1;
    }

    logMe("Score: %d %d\n",
          _clients[Consts::FIRST_CLIENT].rating(), _clients[Consts::LAST_CLIENT].rating());

    logMe("Points: %d %d\n",
          points[Consts::FIRST_CLIENT], points[Consts::LAST_CLIENT]);
}
