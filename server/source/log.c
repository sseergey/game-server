#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>


enum Consts {
    LOG_FILENAME_SIZE = 256,
    BUFFER_SIZE = 1024
};

static char log_filename[LOG_FILENAME_SIZE] = "stderr";
static char buffer[BUFFER_SIZE] = { 0 };


void logInit(const char *filename)
{
    assert(filename && filename[0] != 0);
    strncpy(log_filename, filename, LOG_FILENAME_SIZE - 1);
}


__attribute__((format(printf, 1, 2)))
void logMe(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vsnprintf(buffer, sizeof(buffer), fmt, args);
    va_end(args);

    bool is_stderr = (strncmp(log_filename, "stderr", LOG_FILENAME_SIZE - 1) == 0);
    FILE *f = (is_stderr) ? stderr : fopen(log_filename, "a");
    assert(f);
    fprintf(f, "%s", buffer);
    if (!is_stderr) {
        fclose(f);
    }
}
