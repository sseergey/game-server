#ifndef SERVER_H
#define SERVER_H

#include <game-server-library/messages.h>
#include <vector>


static const char LOG_FILENAME[] = "game-server.log";
const unsigned int height = 20;
const unsigned int width = 20;
const Cell startPosClientOne = { 0, 0 };
const Cell startPosClientTwo = { width-1, height-1 };


class Server
{
public:
    enum Consts {
        CLIENT_COUNT = 2,
        FIRST_CLIENT = 0,
        LAST_CLIENT = 1,
        FOOD_EAT_RATING = 20,
        STANDARD_TURNS_COUNT = 100,
        FOOD_START_COUNT = 10,
        FOOD_ID = 1,
    };

public:
    Server();

    // Changes server's data
    bool isGameEnd() const { return _turn == 0; }
    bool makeTurn();

protected:
    // Client -> Server
    const MSG_ClientMove* receiveClientMove();
    void sendChangesClient(const MSG_GameStatus &message);

    // Server -> Client
    void SendLand(const MSG_Land &message);

    bool changeData(const MSG_ClientMove * const client_moves[]);
    bool moveAndEat(const MSG_ClientMove* client_move);
    void resolveCollision(const MSG_ClientMove * const client_moves[]);
    void eatenFoodCleanup(const MSG_ClientMove* client_move);

    // Data
    void generateInitialFood();

    // Game over
    void finalizeGame() const;

private:
    unsigned int _turn;
    Cells _food;
    Player _clients[Consts::CLIENT_COUNT];
};

#endif // SERVER_H
