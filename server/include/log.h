#pragma once

#ifdef __cplusplus
extern "C" {
#endif

void logInit(const char *filename);
void logMe(const char *fmt, ...) __attribute__((format(printf, 1, 2)));

#ifdef __cplusplus
}
#endif
