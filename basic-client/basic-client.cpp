#include <game-server-library/messages.h>
#include <iostream>

int main() {

    // Receive land
    std::string msgL;
    getline(std::cin, msgL);

    // Gets LandMessage
    const MSG_Land* land = static_cast<const MSG_Land* >(Message::createForClient(msgL));

    // BoardSize must be set
    Size land_size(land->x(), land->y());
    Cell::setBoardSize(land_size);

    // TODO: Handle the land data

    // Release message memory
    if (land) {
        delete land;
    }

    unsigned int turn = 0;
    do {
        // Reads
        std::string msgG;
        getline(std::cin, msgG);

        // GetsGameStatusMessage
        // It's not all message - there are some more interesting things !
        const MSG_GameStatus* game = static_cast<const MSG_GameStatus* >(Message::createForClient(msgG));
        turn = game->turn();

        // Sets move parameters
        Message::Actions action = Message::Actions::MOVE;
        Direction::Dir direction = Direction::Dir::STOP;
        MSG_ClientMove client_move(action, direction);

        // Sends move
        std::cout << client_move.serialize() << std::endl;

        // Release message memory
        if (game) {
            delete game;
        }

    } while (turn != 0);

    // Game Ends
    return 0;
}
